"""
API Wrapper to query Bravo Status for all modes.
"""

import sys
import time
import yaml
import argparse
import os
from bravo_status import BravoStatus
from pybravo import PacketID, DeviceID
from utils import bcolors


REALTIME_DESIRED_PACKETS = [
    PacketID.MODE,
    PacketID.VELOCITY,
    PacketID.POSITION,
    PacketID.CURRENT,
    PacketID.TEMPERATURE,
]
STARTUP_DESIRED_PACKETS = [
    PacketID.SERIAL_NUMBER,
    PacketID.MODEL_NUMBER,
    PacketID.SOFTWARE_VERSION,
    PacketID.HEARTBEAT_FREQUENCY,
    PacketID.POSITION_LIMITS,
    PacketID.VELOCITY_LIMITS,
    PacketID.CURRENT_LIMITS,
]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Check status of Bravo Arm")
    parser.add_argument(
        "file_path", type=str, help="Path to the bravo limits config file"
    )

    args = parser.parse_args()
    BRAVO_LIMITS = args.file_path
    if not os.path.exists(args.file_path):
        print(f"The file '{BRAVO_LIMITS}' does not exist.")
        sys.exit(1)

    status = BravoStatus(REALTIME_DESIRED_PACKETS, STARTUP_DESIRED_PACKETS)
    current_properties = status.get_startup_status()

    while not status.compare_status(current_properties, BRAVO_LIMITS):
        print("Trying to set limits properly...")
        time.sleep(1.0)
    print(
        f"\n{bcolors.OKGREEN}{bcolors.BOLD}All Properties Set! Manipulate Away! {bcolors.ENDC}"
    )
